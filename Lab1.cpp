﻿#pragma once
#include "stdafx.h"
#include <string>
#include "..\CircleLib\Table.h"
#include <iostream>
#include "..\CircleLib\Contribution.h"
using namespace std;



void trans(int k_in, int k_from, double s, table&a);

const char *Names[] = { "Unknown", "valut", "obych", "sroch" };
const char *Menu[] = { "0. Quit","1. open contribution", "2. close contribution",
"3. Show all", "4. modification","5.pereschet","6.perevod" },
*Choice = "Make your choice",
*Msg = "You are wrong; repeate please";

int Answer(const char *alt[], int n);
int open_contrib(table &), ShowAll(table &), close_contrib(table &), mod(table&), pereschet(table&a), perevod(table&a);
int(*Funcs[])(table &) = { NULL, open_contrib, close_contrib, ShowAll,mod,pereschet,perevod };

const int Num = sizeof(Menu) / sizeof(Menu[0]);
const char *Sh1[] = { "1.new summ", "2.na schetu", "3.nachysl", "0.quit" };
const char *Sh[] = { "1.valut", "2.obych", "3.sroch", "0.quit" };
int NumSh = sizeof(Sh) / sizeof(Sh[0]);


int Answer(const char *alt[], int n)
{
	int answer;
	const char *prompt = Choice;
	cout << "What do you want to do:" << endl;
	for (int i = 0; i < n; i++)
		cout << alt[i] << endl;
	do{
		cout << prompt << ": -> ";
		prompt = Msg;
		cin >> answer;
	} while (answer < 0 || answer >= n);
	cin.ignore(80, '\n');
	return answer;
}


void main()
{
	table ar;
	int ind;
	while (ind = Answer(Menu, Num))
		Funcs[ind](ar);
	cout << "That's all. Buy!" << endl;
}

int open_contrib(table&a)
{
	contrib *ptr = nullptr;
	valut v;
	obych o;
	sroch s;
	int num;
	int ans;
	while (ans = Answer(Sh, NumSh)){
		cout << "Enter a number of contribution: --> ";
		cin >> num;
		switch (ans){
		case 1:
			cout << "Enter values : --> ";
			ptr = &v;
			break;
		case 2:
			cout << "Enter : --> ";
			ptr = &o;
			break;
		case 3:
			cout << "Enter : --> ";
			ptr = &s;
			break;
		}
		cin >> (*ptr);
		cin.ignore(80, '\n');
		a.insert(num, ptr);
	}
	return 0;
}




int close_contrib(table&a)
{
	int key;
	int d;
	iter it;
	const contrib *ptr = nullptr;
	cout << "Enter key for delete, please: "<<endl;
	cin >> key;
	it = a.find(key);
	ptr = (*it).second;
	cout << "Enter date: "<<endl;
	cin >> d;
	cout << ptr->getNachisl(d);
	a.erase(key);

	return 0;
}

ostream & operator <<(ostream &os, const pair<const int, contrib  *> &p)
{
	os << '"' << p.first << '"';
	if (p.second)
		os << " - " << (*p.second);
	else
		os << "is empty";
	return os;
}

int ShowAll(table &a)
{
	iter it;
	for (it = a.begin(); it != a.end(); ++it)

		cout << (*it) << endl;
	return 0;
}

int mod(table&a)
{
	iter it;
	int k,d;
	double s1, s2;
	int ans;
	cout << "Enter a number of contribution" << endl;
	cin>> k;
	it = a.find(k);
	while (ans = Answer(Sh1, NumSh)){
		switch (ans){
		case 1:
			cout << "Enter new summ" << endl;
			cin >> s1;
			(*it).second->setSum(s1);
			break;
		case 2:
			cout << "Enter summ for snyat`:--> ";
			cin >> s2;
			cout << ((*it).second->getSum() - s2);
			(*it).second->setSum((*it).second->getSum() - s2);
			break;
		case 3:
			cout << "nachysl: --> ";
			cout << "Enter a date last operation:-->";
			cin >> d;
			cout << (*it).second->getNachisl(d);
			break;
		}
	}
	return 0;
	
}

int pereschet(table&a)
{
	iter it;
	valut *t;
	for (it = a.begin(); it != a.end(); ++it)
	{
		t = dynamic_cast<valut*>((*it).second);
		if (t)
			(*t).rub();
	}
	return 0;

}

void trans(int k_in, int k_from, double s,table&a)
{
	iter p_in, p_from;
	p_in = a.find(k_in);
	p_from = a.find(k_from);
	(*p_in).second->setSum((*p_in).second->getSum() + s);
	(*p_from).second->setSum((*p_from).second->getSum() - s);
}

int perevod(table&a)
{
	int k_from, k_in;
	double summ;
	cout << "Enter number contrib from" << endl;
	cin >> k_from;
	cout << "Enter number contrib in" << endl;
	cin >> k_in;
	cout << "Enter summ" << endl;
	cin >> summ;
	trans(k_in, k_from,summ,a);
	return 0;
}
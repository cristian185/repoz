﻿#include "stdafx.h"
#include "..\CircleLib\circle.h"
#include <iostream>
using namespace std;
using namespace vectors;
vec my_vers;
int getInt(int*p);
int orr()
{
	vec a, b;
	cout << "Input vector n, after items of vector a" << endl;
	cin >> a;
	cout << "Input vector n, after items of vector b" << endl;
	cin >> b;
	cout << (a | b) << endl;

	return 0;
}
int andd()
{
	vec a, b;
	cout << "Input vector n, after items of vector a" << endl;
	cin >> a;
	cout << "Input vector n, after items of vector b" << endl;
	cin >> b;
	cout << (a & b) << endl;
	return 0;
}
int nott()
{
	vec a;

	cout << "Input vector n, after items of vector a" << endl;
	cin >> a;
	cout << (~a);
	return 0;
}
int libo()
{
	vec a, b;
	cout << "Input vector n, after items of vector a" << endl;
	cin >> a;
	cout << "Input vector n, after items of vector b" << endl;
	cin >> b;
	cout << (a ^= b);
	return 0;
}
int maker()
{
	vec a;
	cout << "Input vector n, after items of vector a" << endl;
	cin >> a;
	cout << (a.vydel()) << endl;
	return 0;
}
int dialog(const char *msgs[], int N);
const char *msgs[] = { "0.Quit", "1.OR", "2.AND",
"3.(OR)",
"4.NOT",
"5.ZERO" };

const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
int(*fptr[])() = { NULL, orr, andd, libo, nott, maker };
int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;

	do{
		puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";


		for (i = 0; i < N; ++i)
			puts(msgs[i]);
		puts("Make your choice: --> ");

		n = getInt(&rc);
		if (n == 0)
			rc = 0;
	} while (rc < 0 || rc >= N);

	return rc;
}

int getInt(int*p)
{
	int n;
	do
	{
		n = scanf_s("%d[^\n]", p);
		if (n <= 0)
			scanf_s("%*c", p);
	} while (n <= 0);
	scanf_s("%*c");
	return n;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int rc = 0;
	while (rc = dialog(msgs, NMsgs))
	if (fptr[rc]())
		break;
	printf("That's all. Bye!\n");


	return 0;
}


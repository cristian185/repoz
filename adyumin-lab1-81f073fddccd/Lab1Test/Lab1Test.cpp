﻿#include "stdafx.h"
#include "..\CircleLib\Circle.h"
#include "gtest\gtest.h"
#include <cerrno>
#include <cstdio>
using namespace std;
using namespace vectors;

TEST(testing,or)
{
	char res[] = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1' };
	vec a(121);
	vec b(78);
	vec c;
	c = a | b;
	for (int i = 0; i < 32;i++)
		EXPECT_EQ(res[i], c.getmas(i));
}

TEST(testing, and)
{
	char *s1 = { "1011100" };
	char *s2 = { "100" };
	char res[] = { '0','0','0', '0', '1', '0', '0' };
	vec a(s1);
	vec b(s2);
	vec c;
	c = a & b;
	for (int i = 0; i < 7; i++)
		EXPECT_EQ(res[i], c.getmas(i));
}
TEST(testing, vyd)
{

	char *s = { "0010101000" };
	char res[] = { '1', '0', '1', '0', '1' };
	vec a(s);
	vec b;
	b = a.vydel();
	for (int i = 0; i < b.getn(); i++)
		EXPECT_EQ(res[i], b.getmas(i));
}

int _tmain(int argc, _TCHAR* argv[])
{
	double a;
	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	cin >> a;
	return 1;
}
TEST(testing, inputt)
{
	vec a;
	cout << "Input size 4, after 1,0,1,0" << endl;
	cin >> a;
	char res[] = { '1', '0', '1', '0' };
		bool t = cin.good();
		EXPECT_EQ(true, t);
		EXPECT_EQ(4, a.getn());
		for (int i = 0; i < 4; i++)
			EXPECT_EQ(res[i], a.getmas(i));

}

TEST(testing, inputf)
{
	vec a;
	cout << "input size 4, after 1,2,3,4" << endl;
	cin >> a;
	EXPECT_EQ(0, a.getn());
	bool t = cin.fail();
	EXPECT_EQ(true, t);

}
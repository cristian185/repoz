﻿#include "stdafx.h"
#include "Contribution.h"
#include <iostream>
#include <string>
#define _CRT_SECURE_NO_WARNINGS
using namespace std;
using std::string;

double contrib::getSum()const
{
	return sum;
}

void contrib::setSum(double a)
{
	sum = a;
}

void contrib::setPercent(double a)
{
	percent = a;
}

double contrib::getPercent() const
{
	return percent;
}

int contrib::getDateOpen()const
{
	return dateOpen;
}

void contrib::setDateOpen(int a)
{
	dateOpen = a;
}

int valut::getDateLast()const
{
	return dateLast;
}

void valut::setDateLast(int a)
{
	dateLast = a;
}

double valut::getKurs()const
{
	return kurs;
}

double valut::rub()
{
	sum=sum/kurs;
	return (kurs*sum);
}

void valut::setPtr(string a)
{
	ptr = a;
}

string valut::getPtr()const
{
	return ptr;
}

int valut::getDateOpen()const
{
	return dateOpen;
}

void valut::setKurs(double a)
{
	kurs = a;
}


void obych::setDateLast(int a)
{
	dateLast = a;
}

void valut::setDateOpen(int a)
{
	dateOpen = a;
}

int obych::getDateLast()const
{
	return dateLast;
}

double obych::getNachisl(int a)const
{
	return ((a - dateOpen)*percent*sum / 100);
}

double valut::getNachisl(int a)const
{
	return ((a - dateLast)*percent*sum / 100);
}

double sroch::getNachisl(int a)const
{
	return ((a - dateOpen)*percent*sum / 100);
}

int sroch::getSrok()const
{
	return srok;
}

void sroch::setSrok(int a)
{
	srok = a;
}



ostream& sroch::show(ostream& os)const
{
	return os << " Summ vklada: " << sum
		<< "\n" << " Date open: " << dateOpen << "\n" << " Percent: " << percent<<"\n" << " Srok: " << srok;
}

ostream& obych::show(ostream& os)const
{
	return os << " Summ vklada: " << sum
		<< "\n" << " Date open: " << dateOpen << "\n" << " Percent: " << percent << "\n" << " Date last operation: " << dateLast;

}

ostream& valut::show(ostream& os)const
{

	return os << " Summ vklada: " << sum
		<< "\n" << " Date open: " << dateOpen << "\n" << " Percent: " << percent << "\n"  << " Name valut: " << ptr.c_str() << "\n"
		<< " Date last operation: " << dateLast << "\n" << " Kurs: " << kurs;
}

//ostream& contrib::show(ostream& os)const
//{
//	return os << "summ vklada:" << sum
//		<< "\n" << "Enter date open:" << dateOpen << "\n" << "Enter percent:" << percent;
//}

ostream& operator <<(ostream &os, const contrib &p)
{
	return p.show(os);
}

istream& operator >>(istream &is, contrib &p)
{
	return p.get(is);
}

istream& sroch::get(istream& is)
{
	cout << "Enter sum, dateopen, after percent,srok" << endl;
	return is >> sum >> dateOpen >> percent>> srok;
}

istream& obych::get(istream& is)
{
	cout << "Enter sum, dateopen, after percent,datelast" << endl;
	return is >>sum >> dateOpen >> percent >> dateLast;
}

istream& valut::get(istream& is)
{
	cout << "Enter sum, dateopen, after percent,name kurs, datelast, after kurs" << endl;
	return is >>sum >> dateOpen >> percent >> ptr >> dateLast >> kurs;
}

//istream& contrib::get(istream& is)
//{
//	cout << "Enter sum, dateopen, after percent" << endl;
//	return is >> sum >> dateOpen >> percent;
//}

valut & valut::operator = (const valut &c)
{
	sum = c.sum;
	dateOpen = c.dateOpen;
	percent = c.percent;
	ptr=c.ptr;
	kurs=c.kurs;
	dateLast = c.dateLast;
	return *this;
}

sroch & sroch::operator = (const sroch &c)
{
	sum = c.sum;
	dateOpen = c.dateOpen;
	percent = c.percent;
	srok = c.srok;
	return *this;
}

obych & obych::operator = (const obych &c)
{
	sum = c.sum;
	dateOpen = c.dateOpen;
	percent = c.percent;
	dateLast = c.dateLast;
	return *this;
}


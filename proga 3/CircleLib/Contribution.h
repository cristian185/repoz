﻿#include <stdio.h>
#include <math.h>
#include <string.h>
#include<iostream>
#include"MyMap.h"
#pragma once
using namespace std;
using std::string;

class contrib
{

protected:
	double sum;
	int dateOpen;
	double percent;
	virtual ostream& show(ostream&)const=0;
	virtual istream& get(istream&)=0;
public:
	contrib() : sum(0), dateOpen(0), percent(0){};
	contrib(double summ, int dtOp, double perc) : sum(summ), dateOpen(dtOp), percent(perc){};
	double getSum()const;
	void setSum(double);
	int getDateOpen()const;
	void setDateOpen(int);
	void setPercent(double);
	double getPercent() const;
	friend ostream& operator <<(ostream&, const contrib&);//?
	friend istream& operator >>(istream&, contrib&);//?
	virtual contrib* clone()const = 0;
	virtual double getNachisl(int)const=0;
	virtual ~contrib(){};
};

class valut :public contrib
{
protected:
	string ptr;
	double kurs;
	int dateLast;
	virtual ostream& show(ostream&)const;
	virtual istream& get(istream&);

public:
	//valut(double summ, int dtOp, double perc, string pt, double kur, int dtL) :
	valut & valut::operator = (const valut &c);
	void setDateLast(int);
	void setPtr(string);
	int getDateLast()const;
	valut() : kurs(0), dateLast(0){};
	void setKurs(double);
	double getKurs()const;
	string getPtr()const;
	int getDateOpen()const;
	void setDateOpen(int);
	double rub();
	double getNachisl(int)const;
	virtual valut* clone()const
	{
		return new valut(*this);
	}
	~valut(){};

};

class obych :public contrib
{
protected:
	int dateLast;
	virtual ostream& show(ostream&)const;
	virtual istream& get(istream&);
public:
	obych & obych::operator = (const obych &c);
	virtual obych* clone()const
	{
		return new obych(*this);
	}
	obych() :dateLast(0){};
	int getDateLast()const;
	void setDateLast(int);
	double getNachisl(int)const;
	~obych(){};
};
class sroch :public contrib
{

protected:
	int srok;
	virtual ostream& show(ostream&)const;
	virtual istream& get(istream&);

public:
	sroch & sroch::operator = (const sroch &c);
	virtual sroch* clone()const
	{
		return new sroch(*this);
	}
	sroch() :srok(0){};
	void setSrok(int);
	int getSrok()const;
	double getNachisl(int)const;
	~sroch(){};

};


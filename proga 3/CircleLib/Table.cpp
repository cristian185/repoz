#include "stdafx.h"
#include "Table.h"
#include "Contribution.h"
#include "Contribution.h"
#include <iostream>
#include <string.h>
#include"MyMap.h"
#define _CRT_SECURE_NO_WARNINGS

using namespace std;

table::table(const table&a)
{
	Mymap</*const */int, contrib*>::const_Iterator p;
	for (p = a.arr.begin(); p != a.arr.end(); p++);
		arr.insert((*p).first, (*p).second->clone());
}

table::table()
{
	Mymap</*const*/ int, contrib *>::Iterator p;
	for (p = arr.begin(); p != arr.end(); p++)
	{
		delete (*p).second;
		(*p).second = nullptr;
	}
}

table& table::operator = (const table &a)

{
	Mymap</*const*/ int, contrib *>::Iterator p;
	if (this != &a)
	{
		for (p = arr.begin(); p != arr.end(); ++p)
			delete (*p).second;
		arr.clear();
		Mymap</*const*/ int, contrib *>::const_Iterator pp;
		for (pp = a.arr.begin(); pp != a.arr.end(); ++pp);
			arr.insert((*pp).first, (*pp).second->clone());
	}
	return *this;
}


void table::insert(const int &s, const contrib *f)
{
	Mymap</*const*/ int, contrib *>::Iterator p = arr.find(s);
	if (p == arr.end()){
		arr.insert(s, nullptr);
		p = arr.find(s);
	}
		delete (*p).second;
		(*p).second = f->clone();
}

//void table::erase(const int &s)
//{
//	Mymap</*const */int, contrib *>::Iterator p = arr.find(s);
//	if (p == arr.end())
//		throw exception("no this item");
//	else
//	{
//		arr.erase(p);
//		cout << "all right";
//	}
//}



table::iterator table::find(const int &s)
{
	Mymap</*const*/ int, contrib *>::Iterator p = arr.find(s);
	return iter(p);
}

table::iterator table::begin()
{
	return iter(arr.begin());
}

table::iterator table::end()
{
	return iter(arr.end());
}

//methods for class iter

int table::iterator::operator !=(const iter &it) const
{
	return cur != it.cur;
}

int iter::operator ==(const iter &it) const
{
	return cur == it.cur;
}

Pair</*const*/ int, contrib *> & iter::operator *()
{
	return *cur;
}

iter & iter::operator ++()
{
	++cur;
	return *this;
}

iter iter::operator ++(int)
{
	iter res(*this);
	++cur;
	return res;
}


table::~table()
{
	Mymap</*const*/ int, contrib *>::Iterator p;
	for (p = arr.begin(); p != arr.end(); ++p){
		delete (*p).second;
		(*p).second = nullptr;
	}
}




//void table::poisk(int k)
//{
//	map<const int, contrib*>::iterator p;
//	for (p = arr.begin(); p != arr.end(); ++p)
//	{
//		if ((*p).first == k)
//		{
//		valut *t = dynamic_cast<valut*>((*p).second);
//		if (t)
//		{
//
//		}
//		}
//
//	}
//
//}


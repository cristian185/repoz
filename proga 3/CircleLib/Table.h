#ifndef _TABLE_H
#define _TABLE_H
#pragma once
#include <stdio.h>
#include <cmath>
#include <string.h>
#include<iostream>
#include"MyMap.h"
#include "Contribution.h"


class table
{
private:
	Mymap< /*const*/ int, contrib*> arr;

public:
	table();
	table(const table&);
	table& operator=(const table&);
	void insert(const int&, const contrib*);
	//void erase(const int &);
	friend class iter;
	typedef iter iterator;
	iterator begin();
	iterator end();
	iterator find(const int&);
	~table();
	
};

class iter{
private:
	Mymap </*const*/ int, contrib*>::Iterator cur;
public:
	iter(){};
	iter(Mymap</*const*/ int, contrib*>::Iterator it) :cur(it){};
	Pair</*const*/ int, contrib*>&operator *();
	iter&  operator ++();
	iter  operator ++(int);
	int operator !=(const iter&) const;
	int operator ==(const iter&) const;
};


#endif

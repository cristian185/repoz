﻿#include "stdafx.h"
#include "C:\Users\dnsshop\Documents\Visual Studio 2013\Projects\gtest-1.7.0\fused-src\gtest\gtest.h"
#include "..\CircleLib\Contribution.h"
#include "..\CircleLib\Table.h"
#include "..\CircleLib\MyMap.h"
#include <string>



using namespace std;

string nazvKursa[] = { "Dollar", "Euro", "", "Pound", "Yen", "Franc", "Bolivar"};

int dtlast[] = {86,75,105,93,94,90 };
int dtopen[] = { 10, 15, 30, 12, 60 };
double perc[] = { 10, 13, 15, 11, 7 };
double krs[] = { 35, 63, 45, 52, 67, 84 };
double sum[] = { 1288, 255, 1463, 228, 3698 };
int srok[] = { 12, 16, 18, 21, 50 };


table tb;
const int n = 30;

int mas1_10000[n];         //для шифра, кол-ва, индекса

int range = 9999; int min = 1;

void h()
{
	for (int i = 0; i < n; ++i)					//заполнение рэндомами от 1 до 10000// для шифра кол-ва изд, индекса;
	{
		mas1_10000[i] = rand() % (range + 1) + min;
	}
}



TEST(add, 1)
{
	int  date_last, date_open,kolvo, srokk, index, shifr;
	string Ptr;
	double percen, summa, kurss;
	//int ans;
	h();
	int masSrok[n];
	double masKurs[n];
	int masDateLast[n];
	double masSum[n];
	int masDateOpen[n];
	double masPercent[n];
	int mas1_3[n];
	string masPtr[n];

	srand(time(NULL));


	range = 6;
	min = 0;

	for (int i = 0; i < n; ++i){
		int ind = rand() % (range + 1) + min;
		masPtr[i] = nazvKursa[ind];   //названия  курса
	}
	range = 3;
	min = 0;

	for (int i = 0; i < n; ++i){
		masSum[i] = sum[rand() % (range + 1) + min];  //сумма
	}
	range = 3;
	min = 0;

	for (int i = 0; i < n; ++i){
		masDateOpen[i] = dtopen[rand() % (range + 1) + min];   //открытие
	}

	for (int i = 0; i < n; ++i){
		masDateLast[i] = dtlast[rand() % (range + 1) + min];   //закрыие
	}

	for (int i = 0; i < n; ++i){
		masKurs[i] = krs[rand() % (range + 1) + min];   // курс
	}

	for (int i = 0; i < n; ++i){
		masPercent[i] = perc[rand() % (range + 1) + min];   //проценты
	}

	for (int i = 0; i < n; ++i){
		masSrok[i] = srok[rand() % (range + 1) + min];   //срок
	}

	range = 2; min = 1;
	for (int i = 0; i < n; ++i) { //заполнение рэндомами от 1 до 3  для выбора включаемого элемента и кол-ва курсов;
		mas1_3[i] = rand() % (range + 1) + min;
		
	}





	for (int i = 0; i < n; i++){
		contrib *ptrr = nullptr;

		valut v;
		obych o;
		sroch s;
		kolvo = mas1_10000[i], index = mas1_10000[i];
		int ans = mas1_3[i];
		summa = masSum[i];
		date_open = masDateOpen[i];
		percen = masPercent[i];
		kurss = masKurs[i];
		date_last = masDateLast[i];
		Ptr = masPtr[i];
		srokk = masSrok[i];

		switch (ans){
		case 1:

			v.setSum(summa);
			v.setDateOpen(date_open);
			v.setDateLast(date_last);
			v.setKurs(kurss);
			v.setPtr(Ptr);
			v.setPercent(percen);
			ptrr = &v;

			shifr = mas1_10000[i];

			break;

		case 2:
			
			o.setSum(summa);
			o.setDateOpen(date_open);
			o.setDateLast(date_last);
			o.setPercent(percen);
			
			shifr = mas1_10000[i];
			ptrr = &o;



			break;
		case 3:
			s.setSum(summa);
			s.setPercent(percen);
			s.setSrok(srokk);
			s.setDateOpen(date_open);
			ptrr = &s;
			shifr = mas1_10000[i];
			break;
		}

		tb.insert(shifr, ptrr);
	}

	/*for (int i = 0; i < n; i++){
		tb[mas1_10000[i]]->setkolvo(tb[mas1_10000[i]]->getkolvo() - 1);
		}
		int good = 8888;
		for (int i = 0; i < n; i++){
		if (tb[mas1_10000[i]]->getkolvo() != mas1_10000[i] - 1)
		good = 0;

		}
		cout << good;

		for (int i = 0; i < n; i++){
		tb[mas1_10000[i]]->setkolvo(tb[mas1_10000[i]]->getkolvo() + 1);
		}
		for (int i = 0; i < n; i++){
		if (tb[mas1_10000[i]]->getkolvo() != mas1_10000[i])
		good = 0;

		}
		cout << good;*/

	valut *vt;
	obych *ot;
	sroch *st;
	iter it;
	const contrib *ptrrr = nullptr;
	for (int i = 0; i < n; i++){
		it = tb.find(mas1_10000[i]);
		vt = dynamic_cast<valut*>((*it).second);
		ot = dynamic_cast<obych*>((*it).second);
		st = dynamic_cast<sroch*>((*it).second);
		if (vt)
		{
			EXPECT_EQ(vt->getSum(), masSum[i]);
			EXPECT_EQ(vt->getDateOpen(), masDateOpen[i]);
			EXPECT_EQ(vt->getPercent(), masPercent[i]);
			EXPECT_EQ(vt->getDateLast(), masDateLast[i]);
			EXPECT_EQ(vt->getKurs(), masKurs[i]);
			EXPECT_EQ(vt->getPtr(), masPtr[i]);
		}

		if (ot)
		{
			EXPECT_EQ(ot->getSum(), masSum[i]);
			EXPECT_EQ(ot->getDateOpen(), masDateOpen[i]);
			EXPECT_EQ(ot->getPercent(), masPercent[i]);
			EXPECT_EQ(ot->getDateLast(), masDateLast[i]);
		}

		if (st)
		{
			EXPECT_EQ(st->getSum(), masSum[i]);
			EXPECT_EQ(st->getDateOpen(), masDateOpen[i]);
			EXPECT_EQ(st->getPercent(), masPercent[i]);
			EXPECT_EQ(st->getSrok(), masSrok[i]);
		}

	}
}


ostream & operator <<(ostream &os, const pair<int, contrib *> &p)
{
	cout << endl;

	os << '"' << p.first << '"';
	if (p.second)
		os << " - " << (*p.second);
	else
		os << "is empty";

	return os;
}

TEST(vivod, 1)
{
	iter it;
	for (it = tb.begin(); it != tb.end(); ++it)
		cout << (*it) << endl;
}

//}
//
//TEST(udalenie, 1)
//{
//	for (int i = 0; i < n; i++)
//		tb.udal(mas1_10000[i]);
//
//
//}
//
//
//TEST(throws, 1)
//{
//	uchebnoe uch;
//
//
//	for (int i = 0; i < n; i++)
//		ASSERT_THROW(tb.udal(mas1_10000[i]), exception);
//
//	for (int i = 0; i < n; i++)
//		ASSERT_THROW(tb.find(mas1_10000[i]), exception);
//
//
//}

TEST(nothrows, 1){

	ASSERT_NO_THROW(valut v);
	ASSERT_NO_THROW(obych o);
	ASSERT_NO_THROW(sroch s);

	valut v;
	EXPECT_EQ(v.getSum(), 0);
	EXPECT_EQ(v.getDateOpen(), 0);
	EXPECT_EQ(v.getDateLast(), 0);
	EXPECT_EQ(v.getPtr(), "");
	EXPECT_EQ(v.getKurs(), 0);
	EXPECT_EQ(v.getPercent(), 0);

	obych o;
	EXPECT_EQ(o.getSum(),0 );
	EXPECT_EQ(o.getDateOpen(), 0);
	EXPECT_EQ(o.getDateLast(), 0);
	EXPECT_EQ(o.getPercent(), 0);
	
	sroch s;
	EXPECT_EQ(s.getSum(), 0);
	EXPECT_EQ(s.getDateOpen(), 0);
	EXPECT_EQ(s.getPercent(), 0);
	EXPECT_EQ(s.getSrok(), 0);
}





int main(int argc, char * argv[]) {
	int j;
	::testing::InitGoogleTest(&argc, argv);

	cout << RUN_ALL_TESTS();


	cin >> j;
	return 0;
}